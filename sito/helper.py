# coding=utf-8
from datetime import *


from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import ListView, DetailView
from django.template.loader import render_to_string
from product.models import *
from product.forms import *
from sito.models import *
from order.models import *
from django.core.mail import send_mail
from filer.models import *
#
#login
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.shortcuts import redirect

from django.db.models import Sum
from decimal import *
from decimal import Decimal

from django.contrib import messages








# da che data a che data si visualizzerà l'inverno Da FEBBRAIO A FINE AGOSTO
# da che data a che data si visualizzerà l'estate DA SETTEMBRE A FINE GENNAIO
def get_stagione(inizio, fine):
    oggi = datetime.now().date()
    if inizio < oggi < fine:
        return "INVERNO"
    else:
        return "ESTATE"


'''
inizio = date(2017, 02, 01)
fine = date(2017, 8, 30)
stagione = get_stagione(inizio, fine)
print stagione
'''


# controlla magazzino e scarica
def check_magazzino(qt_richiesta, qt_magazzino, compi):
    if qt_magazzino > 0 or qt_magazzino >= qt_richiesta:
        print "quantita richiesta ok"
        quantita = qt_magazzino - qt_richiesta
        if quantita >=0:
            compi.quantity = quantita
            compi.save()
            return True
        else:
            return False
    else:
        #order.delete()
        return False


#print check_magazzino(3, 32)


# trasporto italia fedex
def trasporto(peso):
    if peso >= Decimal(0,5) and peso <= Decimal(2,5):
        return Decimal(37,12)
    elif peso >= Decimal(2,5) and peso  <= Decimal(10):
        return Decimal(47,97)
    elif peso > Decimal(10) and peso  <= Decimal(20,5):
        return Decimal(74,24)
    elif peso > Decimal(20,5) and peso <= Decimal(34,5):
        return Decimal(94,15)
    elif peso > Decimal(34,5) and peso <= Decimal(44,5):
        return Decimal(111,69)
    elif peso > Decimal(44,5) and peso <= Decimal(54,5):
        return Decimal(133,40)
    elif peso > Decimal(54,5) and peso <= Decimal(64,5):
        return Decimal(151,52)
    elif peso > Decimal(64,5) and peso <= Decimal(70,5):
        return Decimal(169,7)
    else:
        return peso * Decimal(2,47)

def totale_trasporto(cart):
    x = Decimal(0,0)
    for c in cart.items:
        x += trasporto(Decimal(c.product.weight)) * c.quantity
    return Decimal(x)
