# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-06-28 11:03
from __future__ import unicode_literals

from django.db import migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0009_remove_moduli_thumb_moduli'),
    ]

    operations = [
        migrations.AddField(
            model_name='moduli',
            name='thumb_moduli',
            field=image_cropping.fields.ImageRatioField('image', '400x400', adapt_rotation=False, allow_fullsize=False, free_crop=False, help_text=None, hide_image_field=False, size_warning=True, verbose_name='400x400'),
        ),
    ]
